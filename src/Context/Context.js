import React, {createContext, useState} from 'react'

export const Context = createContext();

const ContextProvider = (props) => {

    const [theme, setTheme] = useState(false);

        const toggleTheme = () => {
            setTheme(!theme);
        }

        if(theme){
            document.body.classList.add('dark-body');
        } else {
            document.body.classList.remove('dark-body');
        }

    return (
        <Context.Provider value={{toggleTheme, theme}}>
            {props.children}
        </Context.Provider>
    )
}

export default ContextProvider;

  
