import React, {useContext} from 'react';
import './BtnToggle.css';
import {Context} from '../../Context/Context'


export default function BtnToggle() {

    const {toggleTheme, theme} = useContext(Context);


  return (
    <button onClick={toggleTheme}
     className={theme ? 'btn-toggle' : 'btn-toggle dark-btn'}>
         {theme ? 'Light' : 'Dark'}
    </button>
  )
}
