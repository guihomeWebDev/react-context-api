import './App.css';
import Content from './Components/Content/Content';
import ContextProvider from './Context/Context';
function App() {
  return (
    <div className="App">
      <ContextProvider>
        <Content />
      </ContextProvider>
    </div>
  );
}

export default App;
